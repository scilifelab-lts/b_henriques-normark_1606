# B_Henriques-Normark_1606

Repository for **Pneumonial metagenomics, metatranscriptomics and metabolomics**
project.

## Overview

The B_Henriques-Normark_1606 project uses RNAseq samples from human patients, 
sequenced with Illumina technology. The first pilot study gave some
indication on how to best perform library preparation and sequencing in order
to detect both human and microbial gene expression. The 100h extension to the 
project will focus on 42 samples prepared using the SMARTer Pico RNA kit and 
sequenced with NovaSeq6000 S2, PE 2X150bp. The setup is as follows:

## Methods

1. **ON BIANCA**

    1.1 Read pre-processing (using rnaseq workflow v1.2).

    1.2 Sort pre-processed reads into rRNA/non_rRNARNA using SortMeRNA using 
    all rRNA databases.
    
    1.3 Run the [nf-core/rnaseq](https://github.com/nf-core/rnaseq/) (v1.3)
    pipeline on the preprocessed mRNA reads. The workflow was run with settings
    `pico = true aligner = 'star' genome = 'GRCh37'`.
    
    1.4 Run SortMeRNA on the pre-processed reads from step 1.2 using only the 
    eukaryotic rRNA databases. Keep reads not classified as rRNA in this step.  

2. **ON RACKHAM**

    2.1 Reads for which at least one read-pair did not map to the human 
    reference genome in step 1.3 were treated as non-human `non_rRNA` and 
    classified using kraken2. These were processed using the 
    [config_non_rRNA.yaml](config_non_rRNA.yaml) config file:
    `snakemake --configfile config_non_rRNA.yaml --use-conda`
    
    2.2 Reads not classified as human rRNA in step 1.4 were treated as non-human
    `rRNA` and classified using kraken2. These were processed using the 
    [config_rRNA.yaml](config_rRNA.yaml) config file:
    `snakemake --configfile config_rRNA.yaml --use-conda`
    
    2.3 Kraken report files were loaded into [pavian](https://github.com/fbreitwieser/pavian)
     (v0.8.4) and species tables were exported.

### Docker images

Two Docker images are available to recreate the software environments used in 
steps 1.2 and 1.4 (Bianca) as well as steps 2.1 and 2.2 (Rackham).
 
To pull and start an interactive shell in the Bianca software environment do:

```bash
docker run --rm -it normarklab/normark-1606:bianca
```

And for the Rackham environment:

```bash
docker run --rm -it normarklab/normark-1606:rackham
```

### Jupyter notebook

The Jupyter notebook [Analysis.ipynb](Analysis.ipynb) was used to generate 
summary tables and figures and to perform Random forest analysis on output
from the snakemake workflow. The conda environment needed to run the notebook
is found in [environment.yml](environment.yml) and can be created by running:
   
```bash
conda env create -f environment.yml
conda activate b_henriques_normark_1606
```

Activate the environment by running:

```bash
conda activate b_henriques_normark_1606
```

The notebook can then be executed from the commandline using:

```bash
jupyter nbconvert --to HTML --execute Analysis.ipynb
```

#### Run the notebook with Docker
To run the jupyter notebook with docker and create all images in a directory 
called `results/figures` in the current working directory do:

```bash
docker run --rm -v $(pwd)/results/figures:/analysis/results/figures normarklab/normark_1606:jupyter
```