localrules:
    aggregate_bins,
    checkm_bin_qa_plot

rule aggregate_bins:
    input:
        summary = opj(config["results_path"], "binning", "bin","{group}","{group}.summary")
    output:
        flag = touch(opj(config["results_path"], "binning", "bin","aggregated","{group}"))
    params:
        agg_dir = opj(config["results_path"], "binning", "bin","aggregated")
    run:
        from glob import glob
        shell("mkdir -p {params.agg_dir}")
        dir = os.path.dirname(input.summary)

        for bin in glob("{}/*.fasta".format(dir)):
            basename = os.path.basename(bin)
            target = os.path.abspath(bin)
            link = "{}/{}".format(os.path.abspath(params.agg_dir), basename)
            if not os.path.isfile(link):
                shell("ln -s {target} {link}")

rule checkm_analyze:
    """Analyzes bins for completeness and contamination using a set of universal Prokaryotic marker genes"""
    input:
        marker_file = "resources/checkm/Prokaryote.ms",
        flags = expand(opj(config["results_path"], "binning", "bin","aggregated","{group}"), group = assemblyGroups.keys())
    output:
        stats_file = opj(config["results_path"],"checkm","quality","storage","bin_stats.analyze.tsv")
    params:
        bin_dir = opj(config["results_path"], "binning", "bin","aggregated"),
        out_dir = opj(config["results_path"],"checkm","quality")
    message: "Analyzing bins with CheckM"
    conda:
        "../../../envs/checkm.yaml"
    threads: 4
    resources:
        runtime = lambda wildcards, attempt: attempt**2*60*4
    shell:
        """
        checkm analyze -t {threads} -x .fasta {input.marker_file} {params.bin_dir} {params.out_dir}
        """

rule checkm_qa:
    input:
        marker_file = "resources/checkm/Prokaryote.ms",
        stats_file = opj(config["results_path"],"checkm","quality","storage","bin_stats.analyze.tsv")
    output:
        qa_table = opj(config["results_path"],"checkm","genome_stats.tab")
    conda:
        "../../../envs/checkm.yaml"
    params:
        input_dir = opj(config["results_path"],"checkm","quality")
    message: "Generating stats for bins"
    resources:
        runtime = lambda wildcards, attempt: attempt**2*60*1
    shell:
        """
        checkm qa -o 2 --tab_table {input.marker_file} {params.input_dir} > {output.qa_table}
        """

rule checkm_bin_qa_plot:
    input:
        qa_table = opj(config["results_path"],"checkm","genome_stats.tab")
    output:
        opj(config["results_path"],"checkm","bin_qa_plot.png")
    params:
        bin_folder = opj(config["results_path"], "binning", "bin","aggregated"),
        out_folder = opj(config["results_path"],"checkm","quality"),
        plot_folder = opj(config["results_path"],"checkm")
    conda: "../../../envs/checkm.yaml"
    shell:
        """
        checkm bin_qa_plot -x .fasta {params.out_folder} {params.bin_folder} {params.plot_folder}
        """

rule checkm_tree:
    input:
        marker_file = "resources/checkm/Prokaryote.ms",
        flags = expand(opj(config["results_path"], "binning", "bin","aggregated","{group}"), group = assemblyGroups.keys())
    output:
        phylofile = opj(config["results_path"],"checkm","phylogeny","storage","tree","concatenated.tre")
    params:
        out_dir = opj(config["results_path"],"checkm","phylogeny"),
        bin_dir = opj(config["results_path"], "binning", "bin","aggregated")
    conda:
        "../../../envs/checkm.yaml"
    threads: 20
    resources:
        runtime = lambda wildcards, attempt: attempt**2*60*4
    shell:
        """
        rm -rf {params.out_dir}
        checkm tree -x .fasta -t {threads} --pplacer_threads {threads} {params.bin_dir} {params.out_dir}
        """

rule checkm_tree_qa:
    input:
        marker_file = "resources/checkm/Prokaryote.ms",
        phylofile = opj(config["results_path"],"checkm","phylogeny","storage","tree","concatenated.tre")
    output:
        table = opj(config["results_path"],"checkm","phylogeny.tab")
    params:
        input_dir = opj(config["results_path"],"checkm","phylogeny")
    resources:
        runtime = lambda wildcards, attempt: attempt**2*60*1
    conda:
        "../../../envs/checkm.yaml"
    shell:
        """
        checkm tree_qa --tab_table {params.input_dir} > {output.table}
        """