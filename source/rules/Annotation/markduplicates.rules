rule remove_mark_duplicates:
    input:
        opj(config["results_path"],"assembly","{group}","mapping","{sample}_{run}_{seq_type}.bam")
    output:
        opj(config["results_path"],"assembly","{group}","mapping","{sample}_{run}_{seq_type}.markdup.bam"),
        opj(config["results_path"],"assembly","{group}","mapping","{sample}_{run}_{seq_type}.markdup.metrics")
    log:
        opj(config["results_path"],"assembly","{group}","mapping","{sample}_{run}_{seq_type}.markdup.log")
    params:
        temp_bam=opj(config["tmpdir"],"{group}-mapping-{sample}_{run}_{seq_type}.markdup.bam"),
        temp_sort_bam=opj(config["tmpdir"],"{group}-mapping-{sample}_{run}_{seq_type}.markdup.re_sort.bam"),
        jarfile=config["picard_jar"],
        picard_path=config["picard_path"],
        java_opt="-Xms2g -Xmx31g"
    threads: 4
    resources:
        runtime = lambda wildcards, attempt: attempt**2*60*4
    shell:
        """
        java {params.java_opt} -XX:ParallelGCThreads={threads} -cp params.picard_path -jar \
        {params.jarfile} MarkDuplicates I={input} O={params.temp_bam} M={output[1]} \
        ASO=coordinate REMOVE_DUPLICATES=TRUE 2> {log}
        # Re sort the bam file using samtools
        samtools sort -@ 3 -o {params.temp_sort_bam} {params.temp_bam}
        mv {params.temp_sort_bam} {output[0]}
        rm {params.temp_bam}
        """