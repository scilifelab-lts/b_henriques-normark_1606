FROM continuumio/miniconda3:4.8.2

LABEL maintainer="John Sundh" email=john.sundh@nbis.se
LABEL description="Docker image to generate figures for b_henriques_normark_1606"

# Use bash as shell
SHELL ["/bin/bash", "-c"]

# Set workdir
WORKDIR /analysis

RUN apt-get update && \
    apt-get install -y --no-install-recommends curl && apt-get clean

# Add environment file
COPY environment.yml .

# Install environment into base
RUN conda env update -n base -f environment.yml && conda clean -a

# Add repository files
COPY results results
COPY rRNA rRNA
COPY non_rRNA non_rRNA

# Add jupyter notebook
COPY Analysis.ipynb ./

# Expose the port for jupyter
EXPOSE 8888

# Set up entrypoint
ENTRYPOINT ["jupyter", "nbconvert", "--execute", "Analysis.ipynb"]

# Set extra commands
CMD ["--to", "HTML"]