#!/usr/bin/env python

import json
from urllib import request
from argparse import ArgumentParser

import pandas as pd


def get_kegg_hierarchy(url="http://www.kegg.jp/kegg-bin/download_htext?htext=br08901.keg&format=json"):
    response=request.urlopen(url)
    data=response.read().decode('utf-8')
    s=json.loads(data)
    hier = {}
    for cat1,d1 in enumerate(s['children'],start=1):
        for cat2,d2 in enumerate(d1['children'],start=1):
            for p in d2['children']:
                item = p['name']
                pathway = "map"+item.split(" ")[0]
                name = " ".join(item.split(" ")[1:])
                hier[pathway] = {"name": name, "Cat1": d1['name'], "Cat2": d2['name']}
    hier_df = pd.DataFrame(hier).T
    return hier_df


def get_pathway(l, level=0):
    pathways = []
    for item in l:
        try:
            pathways.append(item.replace("PATHWAY: ","").split(";")[level].lstrip().split(".")[0])
        except IndexError:
            pathways.append("Unknown")
    return pathways


def get_toplevel(hier_df, path2):
    path1=[]
    for p in path2:
        try:
            path1.append(list(set(hier_df.loc[hier_df.Cat2==p]["Cat1"]))[0])
        except IndexError:
            path1.append("Unknown")
    return path1


def main():
    parser = ArgumentParser()
    parser.add_argument("-i", "--infile", type=str, required=True,
                        help="Infile with gene abundances.")
    parser.add_argument("-p", "--pathwaymap", type=str, default='Pathway',
                        help="Pathway column name. Defaults to 'Pathway'")
    parser.add_argument("-c", "--commoncols", default=['Gene Name','Reference','Strand','Start','End'], nargs="+",
                        help="Common columns to exclude from grouping and quantification")

    args = parser.parse_args()

    #################
    # Read KEGG map #
    #################
    map_df = pd.read_table(args.pathwaymap, header=0, index_col=0, usecols=[0, 2])
    map_df = map_df.loc[map_df["KEGG Pathway and Enzyme ID"] == map_df["KEGG Pathway and Enzyme ID"]]
    rename_cols = {"KEGG Pathway and Enzyme ID": "pathway"}
    pathways = []
    map_df.rename(columns=rename_cols,inplace=True)
    for item in map_df.pathway:
        p = item.split("+")[0]
        pathways.append("map{}".format(p))
    map_df["pathway"] = pathways
    # Get hierarchy
    hier_df = get_kegg_hierarchy()
    map_df = pd.merge(hier_df, map_df, left_index=True, right_on="pathway")


    ########################
    # Read gene abundances #
    ########################
    df=pd.read_table(args.infile, header=0, index_col=0)

    # Drop common columns
    df.drop(args.commoncols, axis=1, errors="ignore", inplace=True)

    # Merge with mapping
    df_mapped=pd.merge(map_df,df,left_index=True,right_index=True)
    df_mapped.drop("pathway", axis=1, inplace=True)
    # Sum to category
    df_summed=df_mapped.groupby(["Cat1","Cat2","name"]).sum().reset_index()

    sample_cols=df_summed.drop(["Cat1","Cat2","name"], axis=1).columns
    krona_commands={}
    for item in sample_cols:
        sample_name=".".join(item.split(".")[0:-1])
        q=item.split(".")[-1]
        if not q in krona_commands.keys():
            krona_commands[q] = "ktImportText -o Krona.{}.html ".format(q)
        df_out=df_summed.loc[:, [item,"Cat1","Cat2","name"]]
        file="{}.tab".format(item)
        krona_commands[q] += "{},{} ".format(file,sample_name)
        df_out.to_csv(file, sep="\t", index=False, header=False)
    print("Generate krona plots using:")
    for key,val in krona_commands.items():
        print(val)

if __name__ == '__main__':
    main()