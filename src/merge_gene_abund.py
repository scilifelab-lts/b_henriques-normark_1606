#!/usr/bin/env python

import pandas as pd, os, sys
from argparse import ArgumentParser

def main():
    parser = ArgumentParser()
    parser.add_argument("-i", "--infiles", nargs="+", required = True,
                        help="Input files to merge")
    parser.add_argument("-s", "--strip", type=str,
                        help="Text to strip from input files to generate sample names")
    parser.add_argument("-c", "--commoncols", default=['Gene Name','Reference','Strand','Start','End'], nargs = "*",
                        help="Which columns are common to the input files (excluding the first column). Defaults to 'Gene Name' 'Reference' 'Strand' 'Start' 'End'")
    parser.add_argument("-o", "--outfile", type=str,
                        help="Write merged tables to outfile")
    args = parser.parse_args()

    merged = pd.DataFrame()
    common = pd.DataFrame()

    for i,f in enumerate(args.infiles):
        basename = os.path.basename(f)
        if args.strip:
            sample_name = basename.replace(args.strip, "")
        else:
            sample_name = basename
        df = pd.read_table(f, header=0, index_col=0)
        sample_specific_cols = set(df.columns).difference(args.commoncols)
        df_specific = df.loc[:, sample_specific_cols]



        common_cols = [x for x in args.commoncols if x in df.columns]
        df_common = df.loc[:, common_cols]

        rename_cols = {}
        for c in sample_specific_cols:
            rename_cols[c] = "{}.{}".format(sample_name, c)
        df_specific.rename(columns=rename_cols, inplace=True)

        #TODO: This summing is a way to remove duplicate index entries but may not be the correct way to handle it
        df_specific = df_specific.groupby(level=0).sum()

        if i == 0:
            merged = df_specific.copy(deep=True)
            common = df_common.copy(deep=True)
        else:
            merged = pd.merge(merged, df_specific, left_index=True, right_index=True, how="outer")
            new_index = set(df_common.index).difference(set(common.index))
            if len(new_index) > 0:
                common = pd.concat([common,df_common.loc[new_index]])
    df_merged = pd.merge(common, merged, left_index=True, right_index=True)

    if args.outfile:
        fh = args.outfile
    else:
        fh = sys.stdout
    df_merged.to_csv(fh, sep="\t")

if __name__ == '__main__':
    main()