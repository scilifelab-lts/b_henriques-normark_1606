#!/usr/bin/env python

from argparse import ArgumentParser
import pandas as pd
import sys

def return_gene_value_lists(annot_df, col):
    g=[]
    f=[]
    for i in annot_df.index:
        res = annot_df.loc[i, col]
        if type(res) == str:
            values=[res]
        else:
            values=list(set(res.values))
        for gene in i.split(","):
            for v in values:
                g.append(gene)
                f.append(v)
    return g, f


def main():

    parser = ArgumentParser()
    parser.add_argument("-i", "--infile", type=str, required = True,
                        help="File with Ensembl gene ids and some count columns or similar")
    parser.add_argument("-a", "--annotations", type=str, required = True)
    parser.add_argument("-c", "--column", default="Pathway",
                        help="Column from annotation file to add.")
    parser.add_argument("-o", "--outfile", type=str,
                        help="Merged annotated file output")

    args = parser.parse_args()

    annot_df = pd.read_table(args.annotations, header=0, index_col=0)
    annot_df = annot_df.loc[:, [args.column]]
    annot_df = annot_df.loc[annot_df[args.column] == annot_df[args.column]]
    g,f=return_gene_value_lists(annot_df, args.column)

    annot_df = pd.DataFrame(data={"GeneID":g, args.column:f})

    gene_df = pd.read_table(args.infile, header=0, index_col=0)

    merged_df = pd.merge(annot_df, gene_df, left_on="GeneID", right_index=True)

    if args.outfile:
        fh = args.outfile
    else:
        fh = sys.stdout

    merged_df.to_csv(fh, sep="\t", index=False)

if __name__ == '__main__':
    main()
